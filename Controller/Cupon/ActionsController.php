<?php

namespace Nitra\CuponBundle\Controller\Cupon;

use Admingenerated\NitraCuponBundle\BaseCuponController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ActionsController extends BaseActionsController
{
    /**
     * @Route("/action_cupon_autocomplete", name="Nitra_CuponBundle_Cupon_autocomplete")
     * @param Request $request
     */
    public function actionManagementAutocompleteProductAction(Request $request)
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->field('isActive')->equals(true)
            ->limit(20);
        
        $term = $request->query->get('term');
        
        foreach (explode(' ', $term) as $key => $word) {
            if (trim($word)) {
                $Word = preg_quote($word);
                $qb ->addAnd($qb->expr()
                    ->field('fullNameForSearch')->equals(new \MongoRegex("/$Word/i"))
                );
            }
        }
        
        $result = array();
        foreach ($qb->getQuery()->execute() as $key => $product) {
            $result[$key] = $product->getFullNameForSearch();
        }
        
        return new JsonResponse($result);
    }
}