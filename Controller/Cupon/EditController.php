<?php

namespace Nitra\CuponBundle\Controller\Cupon;

use Admingenerated\NitraCuponBundle\BaseCuponController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    /**
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\CuponBundle\Document\Cupon $Cupon
     */
    public function preSave($form, $Cupon)
    {
        $Cupon->setProducts(array_values($Cupon->getProducts()));
    }
}