<?php

namespace Nitra\CuponBundle\Controller\Cupon;

use Admingenerated\NitraCuponBundle\BaseCuponController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    /**
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\CuponBundle\Document\Cupon $Cupon
     */
    public function preSave($form, $Cupon)
    {
        $Cupon->setProducts(array_values($Cupon->getProducts()->toArray()));
    }
}