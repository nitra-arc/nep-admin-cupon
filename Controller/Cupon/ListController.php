<?php

namespace Nitra\CuponBundle\Controller\Cupon;

use Admingenerated\NitraCuponBundle\BaseCuponController\ListController as BaseListController;

class ListController extends BaseListController
{
    protected function getQuery()
    {
        $query = $this->buildQuery();
        $session = $this->getRequest()->getSession();
        //выводим купоны текущего магазина и купоны не относящиеся ни к одному из магазинов
        $query->addOr($query->expr()->field('stores.$id')->equals(new \MongoId($session->get('store_id'))));
        $query->addOr($query->expr()->field('stores')->exists(false));

        $this->processSort($query);
        $this->processFilters($query);
        $this->processScopes($query);

        return $query;
    }
}