# CuponBundle

## Описание

данный бандл предназначен для:

* **создания купонов на скидку**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-cuponbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\CuponBundle\NitraCuponBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
nitra_cupon:
    resource:    "@NitraCuponBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
cupon:
    translateDomain: 'menu'
    route: Nitra_CuponBundle_Cupon_list
```

## Подключение стилей для genemu_jqueryautocomplete_document

```twig
{# app/Resources/views/base_admin.html.twig #}
{% block stylesheets %}
    {{ parent() }}
    <link rel="stylesheet" href="{{ asset('components/jquery-ui/themes/ui-lightness/jquery-ui.min.css') }}" type="text/css" media="all" />
{% endblock stylesheets %}
```