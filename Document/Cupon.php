<?php

namespace Nitra\CuponBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document
 */
class Cupon
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Nitra\StoreBundle\Traits\LocaleDocument;

    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    private $name;

    /**
     * @ODM\String
     * @Gedmo\Translatable
     */
    private $description;

    /**
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    private $code;

    /**
     * Тип купона
     * @ODM\String
     */
    private $type;

    /**
     * До какой суммы купон действителен
     * @ODM\Int
     * @Assert\Range(min = 0, max = 999999)
     */
    private $limit;

    /**
     * @ODM\Float
     */
    private $discount;

    /**
     * @ODM\Hash
     */
    private $products;

    /**
     * @ODM\ReferenceMany(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    private $categories;

    /**
     * @ODM\Date
     */
    private $dateFrom;

    /**
     * @ODM\Date
     */
    private $dateTo;

    /**
     * @ODM\Boolean
     */
    private $isActive;

    /**
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    private $stores;

    /**
     * Количество использований одного промо-кода
     * @ODM\Int
     */
    private $quantity;

    /**
     * Источник промо-кода
     * (где распространяется)
     * @ODM\String
     */
    private $source;

    /**
     * Компания
     * (к примеру к какому-либо празднику)
     * @ODM\String
     */
    private $company;

    public function __construct()
    {
        $this->products   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Cupon
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return \Cupon
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set code
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return \Cupon
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set limit
     *
     * @param int $limit
     * @return \Cupon
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * Get limit
     *
     * @return int $limit
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Get products
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set products
     * @param array $products
     * @return self
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * Get categories
     * @return Doctrine\Common\Collections\Collection $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function addCategory($category)
    {
        $this->categories->add($category);
        return $this;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function removeCategory($category)
    {
        $this->categories->remove($category);
        return $this;
    }

    /**
     * Set dateFrom
     *
     * @param date $dateFrom
     * @return \Cupon
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return date $dateFrom
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param date $dateTo
     * @return \Cupon
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * Get dateTo
     *
     * @return date $dateTo
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set isActive
     *
     * @param int $isActive
     * @return \Cupon
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return int $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set discount
     * @param int $discount
     * @return self
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * Get discount
     * @return int $discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set stores
     *
     * @param Doctrine\Common\Collections\Collection $stores
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

    /**
     * Add stores
     *
     * @param Nitra\StoreBundle\Document\Store $stores
     */
    public function addStore(\Nitra\StoreBundle\Document\Store $stores)
    {
        $this->stores[] = $stores;
    }

    /**
     * Remove stores
     *
     * @param Nitra\StoreBundle\Document\Store $stores
     */
    public function removeStore(\Nitra\StoreBundle\Document\Store $stores)
    {
        $this->stores->removeElement($stores);
    }

    /**
     * Set quantity
     *
     * @param int $quantity
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * Get quantity
     *
     * @return int $quantity
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return self
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * Get source
     *
     * @return string $source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return self
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Get company
     *
     * @return string $company
     */
    public function getCompany()
    {
        return $this->company;
    }
}