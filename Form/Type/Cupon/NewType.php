<?php

namespace Nitra\CuponBundle\Form\Type\Cupon;

use Admingenerated\NitraCuponBundle\Form\BaseCuponType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;

class NewType extends BaseNewType
{
    protected $options = array();
        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('source', 'genemu_jqueryautocomplete_document', $this->options['source']);
        $builder->add('company', 'genemu_jqueryautocomplete_document', $this->options['company']);
    }
    
    protected function getFormOption($name, array $formOptions)
    {
        if (($name == 'source') || ($name == 'company')) {
            $this->options[$name] = $formOptions;
            $this->options[$name]['class']       = 'Nitra\CuponBundle\Document\Cupon';
            $this->options[$name]['property']    = $name;
            $this->options[$name]['configs']     = array(
                'minLength' => 2,
            );
        }
        
        return $formOptions;
    }
}